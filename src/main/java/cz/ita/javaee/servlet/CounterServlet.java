package cz.ita.javaee.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet (value="/counter",name="counterServlet")
public class CounterServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Object counterObj = req.getSession().getAttribute("counterState");
        if (counterObj == null)
            req.getSession().setAttribute("counterState",1);

        res.getWriter().println(req.getSession().getAttribute("counterState"));
    }
}
