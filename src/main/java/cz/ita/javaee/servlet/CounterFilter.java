package cz.ita.javaee.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter (urlPatterns = {"/counter"}, filterName = "CounterFilter")
public class CounterFilter implements Filter {

    @Override
    public void init(FilterConfig arg0) throws ServletException {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        String element = "counterState";
        Object counterObj = request.getSession().getAttribute(element);

        if (counterObj == null)
            request.getSession().setAttribute(element,1);
        else {
            int counterInt = (int)  request.getSession().getAttribute(element);
            request.getSession().setAttribute(element,counterInt+1);
        }

        chain.doFilter(req,res);
    }

    @Override
    public void destroy() {}
}
